//
//  AppDelegate.h
//  AnimateUITableView
//
//  Created by Andre Vieira on 23/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

