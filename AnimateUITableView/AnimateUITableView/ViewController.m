//
//  ViewController.m
//  AnimateUITableView
//
//  Created by Andre Vieira on 23/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ViewController.h"

typedef NS_ENUM(NSInteger, TableViewSections) {
    TokenSmsSections,
    TokenKeyMakerSections,
    TokenAppSections,
    NumberOfSections
};

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *cellExpanded;

@property BOOL expandSms;
@property BOOL expandKeyMaker;
@property BOOL expandApp;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableView];
    
    self.expandSms = NO;
    self.expandKeyMaker = NO;
    self.expandApp = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UItableView
-(void)initTableView{
    self.tableView.allowsSelection = FALSE;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case TokenSmsSections:
            if(self.expandSms)
                return 104;
            else
                return 44;
        case TokenKeyMakerSections:
            if(self.expandKeyMaker)
                return 104;
            else
                return 44;
        case TokenAppSections:
            if(self.expandApp)
                return 104;
            else
                return 44;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NumberOfSections;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == TokenSmsSections) {
        return [self createTokenSmsTableViewCell:tableView path:indexPath];
    } else if (indexPath.section == TokenKeyMakerSections) {
        return [self createKeyMakerTableViewCell:tableView path:indexPath];
    } else if (indexPath.section == TokenAppSections) {
        return [self createAppTableViewCell:tableView path:indexPath];
    }
    return 0;
}

-(CellTableViewCell*)createTokenSmsTableViewCell:(UITableView *)tableView path:(NSIndexPath *)indexPath{
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}

-(CellTableViewCell*)createKeyMakerTableViewCell:(UITableView *)tableView path:(NSIndexPath *)indexPath{
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}

-(CellTableViewCell*)createAppTableViewCell:(UITableView *)tableView path:(NSIndexPath *)indexPath{
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}


-(void)showHide:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    switch (indexPath.section) {
        case TokenSmsSections:
            self.expandSms = !self.expandSms;
            self.expandKeyMaker = NO;
            self.expandApp = NO;
            break;
        case TokenKeyMakerSections:
            self.expandKeyMaker = !self.expandKeyMaker;
            self.expandSms = NO;
            self.expandApp = NO;
            break;
        case TokenAppSections:
            self.expandApp = !self.expandApp;
            self.expandKeyMaker = NO;
            self.expandSms = NO;
            break;
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
}

@end
