//
//  CellTableViewCell.m
//  AnimateUITableView
//
//  Created by Andre Vieira on 23/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "CellTableViewCell.h"

@interface CellTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *headCell;

@end

@implementation CellTableViewCell

- (void)awakeFromNib {
}

-(IBAction)expandOrHiddenButton:(id)sender{
    if(self.delegate){
        [self.delegate showHide:sender];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
