//
//  ViewController.h
//  AnimateUITableView
//
//  Created by Andre Vieira on 23/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellTableViewCell.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end

