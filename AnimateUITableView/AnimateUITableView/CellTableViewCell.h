//
//  CellTableViewCell.h
//  AnimateUITableView
//
//  Created by Andre Vieira on 23/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CellTableViewCellDelegate <NSObject>

-(void)showHide:(UIButton *)sender;

@end

@interface CellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heigthFooterCell;
@property (strong, nonatomic) id<CellTableViewCellDelegate> delegate;

@end
